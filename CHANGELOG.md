# Changelog

## v0.0.1

提供JSON Wen Token (JWT-RFC7519) 的仓颉语言实现

- 支持JWT构造 (HMAC/RSA/ECDSA签名)
- 支持JWT解码
- 支持JWT完整性校验 (HMAC/RSA/ECDSA验签)
- 支持JWT业务字段校验
